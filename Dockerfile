FROM openjdk:14-alpine
COPY out/artifacts/JavaCoursesHomework2_jar/*.jar /app/JavaCourses.jar
CMD ["java", "-jar", "/app/JavaCourses.jar"]
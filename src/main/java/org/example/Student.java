package org.example;

import lombok.*;

import java.util.Date;

@AllArgsConstructor
@Data public class Student
{
    @NonNull private String name;

    @NonNull private Date dateOfBirth;

    @NonNull private String details;

    @Override
    public int hashCode(){
        return name.hashCode();
    }
}


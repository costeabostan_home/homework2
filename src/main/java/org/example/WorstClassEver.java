package org.example;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class WorstClassEver {

    static final String ELEPHANT_IS_HEALTHY = "This elephant is healthy";
    static final String ELEPHANT_SHOULD_EAT_MORE = "This elephant should eat more";
    static final String ELEPHANT_SHOULD_EAT_LESS = "This elephant should eat less";

    static final String BABY_ELEPHANT_IS_HEALTHY = "This baby elephant is healthy";
    static final String BABY_ELEPHANT_SHOULD_EAT_MORE = "This baby elephant should eat more";
    static final String BABY_ELEPHANT_SHOULD_EAT_LESS = "This baby elephant should eat less";

    static final String PLEASE_PRESS_THE_BUTTON = "please press the button";

    public String worstMethodEver(boolean isPressed, int elephantsAge, double elephantsWeight) {
        if (isPressed) {
            if (elephantsAge >= 2) {
                if (elephantsWeight >= 100 && elephantsWeight <= 300) {
                    return ELEPHANT_IS_HEALTHY;
                } else if (elephantsWeight < 100) {
                    return ELEPHANT_SHOULD_EAT_MORE;
                } else {
                    return ELEPHANT_SHOULD_EAT_LESS;
                }
            } else {
                if (elephantsWeight >= 50 && elephantsWeight <= 90) {
                    return BABY_ELEPHANT_IS_HEALTHY;
                } else if (elephantsWeight < 50) {
                    return BABY_ELEPHANT_SHOULD_EAT_MORE;
                } else {
                    return BABY_ELEPHANT_SHOULD_EAT_LESS;
                }
            }
        } else {
            return PLEASE_PRESS_THE_BUTTON;
        }
    }
}


package org.example;

import java.util.Date;

public class App 
{
    public static void main( String[] args )
    {
        Date date = new Date();
        Student student1 = new Student("Name1",date,"details");
        Student student2 = new Student("Name2",date,"details");
        Student student3 = new Student("Name3",date,"details");

        HashSet<Student> hashSet = new HashSet<>(3);
        hashSet.put(student1);
        hashSet.put(student2);
        hashSet.put(student3);

        System.out.println(hashSet.get(student2).getName());
    }
}

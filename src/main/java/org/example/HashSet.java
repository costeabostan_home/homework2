package org.example;

import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import lombok.AllArgsConstructor;

public class HashSet<T> implements Set<T> {

    private final int hashtableSize;
    private final LinkedList<Entry>[] hashtable;

    public HashSet(int arraySize) {
        hashtable = new LinkedList[arraySize];
        hashtableSize = arraySize;
    }

    public boolean put(T value) {
        int hashCode = value.hashCode();
        int index = hashCode % hashtableSize;
        LinkedList<Entry> items = hashtable[index];
        if (items == null) {
            items = new LinkedList<>();
            Entry item = new Entry(hashCode, value);
            items.add(item);
            hashtable[index] = items;
        } else {
            for (Entry item : items) {
                if (item.key == hashCode) {
                    return false;
                }
            }
            Entry item = new Entry(hashCode, value);
            items.add(item);
        }
        return true;
    }

    public boolean delete(Object object) {
        int index = object.hashCode() % hashtableSize;
        LinkedList<Entry> items = hashtable[index];

        if (items == null)
            return false;

        return items.stream()
            .filter(item -> item.key == object.hashCode())
            .findAny()
            .map(items::remove)
            .isPresent();
    }

    private Optional<T> getOptionalElement (Object object) {
        if (object == null)
            return Optional.empty();

        int index = object.hashCode() % hashtableSize;
        LinkedList<Entry> items = hashtable[index];

        if (items == null)
            return Optional.empty();

        return items.stream()
            .filter(item -> item.key == object.hashCode())
            .map(item -> item.value)
            .findAny();
    }

    public T get(T object) {
        return getOptionalElement(object)
            .orElseThrow(() -> new RuntimeException("could not find element"));
    }

    private List<T> getListOfAllObject() {
       return Arrays.stream(hashtable)
            .flatMap(Collection::stream)
            .map(entry -> entry.value)
            .collect(Collectors.toList());
    }

    @Override
    public int size() {
        return hashtable.length;
    }

    @Override
    public boolean isEmpty() {
        return getListOfAllObject().isEmpty();
    }

    @Override
    public boolean contains(Object o) {
        return getOptionalElement(o).isPresent();
    }

    @Override
    public Iterator<T> iterator() {
        return getListOfAllObject().iterator();
    }

    @Override
    public Object[] toArray() {
        return getListOfAllObject().toArray();
    }

    @Override
    public <T1> T1[] toArray(T1[] a) {
        return getListOfAllObject().toArray(a);
    }

    @Override
    public boolean add(T t) {
        return put(t);
    }

    @Override
    public boolean remove(Object o) {
        return delete(o);
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return getListOfAllObject().containsAll(c);
    }

    @Override
    public boolean addAll(Collection<? extends T> collection) {
        collection.forEach(this::put);
        return true;
    }

    @Override
    public boolean retainAll(Collection<?> collection) {
        return collection.stream()
            .filter(element -> getOptionalElement(element).isPresent())
            .map(this::delete)
            .findAny()
            .isPresent();
    }

    @Override
    public boolean removeAll(Collection<?> collection) {
        collection.forEach(this::delete);
        return true;
    }

    @Override
    public void clear() {
        Arrays.fill(hashtable, new LinkedList<>());
    }

    @AllArgsConstructor
    private class Entry {
        private final int key;
        private final T value;
    }
}

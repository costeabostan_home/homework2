package org.example;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.Date;

import static org.junit.Assert.*;

public class HashSetTest {

    private Student student1;
    private Student student2;
    HashSet<Student> hashSet;

    @Before
    public void init() {
        Date date = new Date();
        student1 = new Student("TestName1",date,"Test");
        student2 = new Student("TestName2",date,"Test");
        hashSet = new HashSet<>(1);
    }

    @Test
    public void testAddElement() {
        hashSet.add(student1);
        assertEquals(1, hashSet.size());
    }

    @Test
    public void testRemoveElement() {
        hashSet.remove(student1);
        assertFalse(hashSet.contains(student1));
    }


    @Test
    public void testAddSameElement() {
        HashSet<Student> hashSet = new HashSet<>(1);
        hashSet.add(student1);
        hashSet.add(student1);
        assertEquals(1, hashSet.size());
    }

    @Test
    public void testContains() {
        HashSet<Student> hashSet = new HashSet<>(1);
        hashSet.add(student1);
        assertTrue(hashSet.contains(student1));
    }

    @Test
    public void testIterableContent() {
        HashSet<Student> hashSet = new HashSet<>(1);
        hashSet.add(student1);
        assertTrue(hashSet.iterator().hasNext());
    }

    @Test
    public void testClearElement() {
        HashSet<Student> hashSet = new HashSet<>(1);
        hashSet.add(student1);
        hashSet.clear();
        assertTrue(hashSet.isEmpty());
    }

    @Test
    public void testContainsList() {
        HashSet<Student> hashSet = new HashSet<>(1);
        hashSet.add(student1);
        assertTrue(hashSet.containsAll(Arrays.asList(student1)));
    }

    @Test
    public void testAddingList() {
        HashSet<Student> hashSet = new HashSet<>(1);
        hashSet.addAll(Arrays.asList(student1, student2));
        assertTrue(hashSet.contains(student1));
        assertTrue(hashSet.contains(student2));
    }

    @Test
    public void testRetainList() {
        HashSet<Student> hashSet = new HashSet<>(1);
        hashSet.add(student1);
        hashSet.add(student2);
        hashSet.retainAll(Arrays.asList(student2));
        assertFalse(hashSet.contains(student2));
    }

    @Test
    public void testArrayConversion() {
        HashSet<Student> hashSet = new HashSet<>(1);
        hashSet.add(student1);
        hashSet.add(student2);
        assertEquals(student1, hashSet.toArray()[0]);
    }
}

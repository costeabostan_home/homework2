package org.example;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class WorstClassEverTest {

    private WorstClassEver worstClassEver;

    @Before
    public void init() {
        worstClassEver = new WorstClassEver();
    }

    @Test
    public void testElephantShouldEatMore() {
        String result = worstClassEver.worstMethodEver(true, 2, 50);
        assertEquals("This elephant should eat more", result);
    }

    @Test
    public void testElephantIsHealthy() {
        String result = worstClassEver.worstMethodEver(true, 2, 300);
        assertEquals("This elephant is healthy", result);
    }

    @Test
    public void testElephantShouldEatLess() {
        String result = worstClassEver.worstMethodEver(true, 2, 400);
        assertEquals("This elephant should eat less", result);
    }

    @Test
    public void testBabyElephantShouldEatLess() {
        String result = worstClassEver.worstMethodEver(true, 1, 100);
        assertEquals("This baby elephant should eat less", result);
    }

    @Test
    public void testBabyElephantIsHealthy() {
        String result = worstClassEver.worstMethodEver(true, 1, 50);
        assertEquals("This baby elephant is healthy", result);
    }

    @Test
    public void testBabyElephantShouldEatMore() {
        String result = worstClassEver.worstMethodEver(true, 1, 40);
        assertEquals("This baby elephant should eat more", result);
    }

    @Test
    public void testShouldShowPressButtonValue() {
        String result = worstClassEver.worstMethodEver(false, 1, 40);
        assertEquals("please press the button", result);
    }
}
